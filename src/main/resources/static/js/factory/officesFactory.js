app.factory('officesFactory', [ '$resource', '$http', 'baseURL', function($resource, $http, baseURL) 
{
	var officesfac = {};
	
	officesfac.findAll = function() 
	{
		return $resource(baseURL + "offices").query();
	};
	
	officesfac.findOne = function(id) 
	{
		return $resource(baseURL + "office/" + id).get();
	};
	
	officesfac.saveEntity = function(o) 
	{
		return $resource(baseURL + 'office').save(JSON.parse(o), function(response){});
	};
	
	officesfac.getOffices = function (pageNumber, size)
	{
		pageNumber = pageNumber > 0 ? pageNumber : 0;
		return $http({
			method : 'GET',
			url : baseURL + 'offices/' + pageNumber + '/' + size
		});
	}

	return officesfac;
}])
var app = angular.module('app', ['ngRoute','ngResource','ui.grid','ui.grid.pagination']);
app.config(function($routeProvider){
    $routeProvider        
        .when('/offices',{
            templateUrl: '/views/offices.html',
            controller: 'officesController'
        })
        .when('/offices/:id',{
            templateUrl: '/views/offices-details.html',
            controller: 'officesDetailsController'
        })
        .when('/offices/edit/:id',{
            templateUrl: '/views/offices-edit.html',
            controller: 'officesEditController'
        })
        .when('/office/add',{
            templateUrl: '/views/offices-add.html',
            controller: 'officesAddController'
        })
        .when('/employees',{
            templateUrl: '/views/employees.html',
            controller: 'employeesController'
        })
        .when('/products',{
            templateUrl: '/views/products.html',
            controller: 'productsController'
        })
        .when('/customers',{
            templateUrl: '/views/customers.html',
            controller: 'customersController'
        })
        .when('/orders',{
            templateUrl: '/views/orders.html',
            controller: 'ordersController'
        })
        .when('/payments',{
            templateUrl: '/views/payments.html',
            controller: 'paymentsController'
        })
        .otherwise(
            { redirectTo: '/'}
        );
});


app.constant("baseURL", "http://localhost:8090/")

app.controller('officesController',['$scope', 'officesFactory', function($scope, officesFactory) 
{
	var paginationOptions = 
	{
			pageNumber : 1,
			pageSize : 10,
			sort : null
	};

	officesFactory.getOffices(paginationOptions.pageNumber, paginationOptions.pageSize)
				  .success(function(data) 
				  {
					  $scope.gridOptions.data = data.content;
					  $scope.gridOptions.totalItems = data.totalElements;
				  });

	$scope.gridOptions = 
	{
			paginationPageSizes : [ 5, 10, 20 ],
			paginationPageSize : paginationOptions.pageSize,
			enableColumnMenus : false,
			useExternalPagination : true,
			columnDefs : [
			{
				name : 'officeCode'
			},
			{
				name : 'city'
			},
			{
				name : 'phone'
			},
			{
				name : 'Action',
				cellTemplate : '<div><button ng-click="grid.appScope.deleteRecord(row.entity.officeCode)" type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">Delete</button></div>'
			} ],
			onRegisterApi : function(gridApi) 
			{
				$scope.gridApi = gridApi;
									gridApi.pagination.on
											.paginationChanged(
													$scope,
													function(newPage, pageSize) {
														paginationOptions.pageNumber = newPage;
														paginationOptions.pageSize = pageSize;
														officesFactory.getOffices(
																		newPage,
																		pageSize)
																.success(
																		function(
																				data) {
																			$scope.gridOptions.data = data.content;
																			$scope.gridOptions.totalItems = data.totalElements;
																		});
													});
								}
			};

			
	$scope.deleteRecord = function(officeId) 
	{
		$scope.recId = officeId;
	};

	$scope.deleteYes = function(officeId) 
	{
		location.reload();
	};

}]);



app.controller('officesDetailsController',[
						'$scope',
						'$location',
						'$routeParams',
						'officesFactory',
						function($scope, $location, $routeParams,officesFactory) 
						{
							$scope.headingTitle = "Oficce detais for office with office code :";
							$scope.jsondata = officesFactory.findOne($routeParams.id);
						} 
]);

app.controller('officesEditController', [ 
                        '$scope', 
                        '$location',
                        '$routeParams', 
                        'officesFactory',
                        function($scope, $location, $routeParams, officesFactory) 
                        {
                        	$scope.headingTitle = "Oficces Edit";
                        	$scope.jsondata = officesFactory.findOne($routeParams.id);
                        } 
]);

app.controller('officesAddController', [
		'baseURL',
		'$resource',
		'$scope',
		'$location',
		'$routeParams',
		'officesFactory',
		function(baseURL, $resource, $scope, $location, $routeParams, officesFactory) 
		{
			$scope.headingTitle = "Oficces Add";

			$scope.addOffice = function() 
			{
				var o = '{"officeCode":"' + $scope.officeCode + '", "city":"'
						+ $scope.city + '","phone":"' + $scope.phone
						+ '","addressLine1":"' + $scope.addressLine1
						+ '", "addressLine2":"' + $scope.addressLine2
						+ '", "state":"' + $scope.state + '", "country":"'
						+ $scope.country + '", "postalCode": "'
						+ $scope.postalCode + '", "territory": "'
						+ $scope.territory + '"}';
				officesFactory.saveEntity(o);
				$location.path('/offices');
			};

		} 
]);